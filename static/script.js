var msalConfig = {
    auth: {
        clientId: "35f47bac-140e-42c0-a104-29cc76c73671",
        authority: "https://login.microsoftonline.com/30665e4f-0916-47ff-a837-d36a713eea18",
        redirectUri : "https://www.insuredmine.info/imsso/msredirect"
    },
    cache: {
        cacheLocation: "localStorage",
        storeAuthStateInCookie: true
    }
};


var graphConfig = {
    graphMeEndpoint: "https://graph.microsoft.com/v1.0/me",
    graphMailEndpoint: "https://graph.microsoft.com/v1.0/me/messages"
};
var requestObj = {
     //scopes: ["Mail.Read","user.read","openid", "profile","email"]
     scopes: ["user.read","openid", "profile","email"]
};

var myMSALObj = new Msal.UserAgentApplication(msalConfig);
// Register Callbacks for redirect flow
myMSALObj.handleRedirectCallback(authRedirectCallBack);
function signIn() {
    console.log("27 signIn start");
    myMSALObj.loginPopup(requestObj).then(function (loginResponse) {
        //Login Success
        console.log("48 loginResponse ",loginResponse)
        showWelcomeMessage();
        acquireTokenPopupAndCallMSGraph();
    }).catch(function (error) {
        console.log(error);
    });
}

function callMSGraph(theUrl, accessToken, callback) {
    console.log("in callMSGraph start")
    console.log("theUrl ",theUrl,"-accessToken-",accessToken,)
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200){
            console.log("this.responseText ",this.responseText)
            callback(JSON.parse(this.responseText));
        }
    }
    xmlHttp.open("GET", theUrl, true); // true for asynchronous
    xmlHttp.setRequestHeader('Authorization', 'Bearer ' + accessToken);
    xmlHttp.send();
}

function acquireTokenPopupAndCallMSGraph() {
    console.log("54 acquireTokenPopupAndCallMSGraph start")
    //Always start with acquireTokenSilent to obtain a token in the signed in user from cache
    myMSALObj.acquireTokenSilent(requestObj).then(function (tokenResponse) {
        console.log("58 tokenResponse: ",tokenResponse)
         callMSGraph(graphConfig.graphMeEndpoint, tokenResponse.accessToken, graphAPICallback);
    }).catch(function (error) {
         console.log("60 error ",error);
         // Upon acquireTokenSilent failure (due to consent or interaction or login required ONLY)
         // Call acquireTokenPopup(popup window)
         if (requiresInteraction(error.errorCode)) {
             myMSALObj.acquireTokenPopup(requestObj).then(function (tokenResponse) {
                 console.log("66 tokenResponse ",tokenResponse)
                 callMSGraph(graphConfig.graphMeEndpoint, tokenResponse.accessToken, graphAPICallback);
             }).catch(function (error) {
                 console.log("error ", error);
             });
         }
    });
}


function graphAPICallback(data) {
    console.log("73 data ",data)
    document.getElementById("json").innerHTML = JSON.stringify(data, null, 2);
}
function showWelcomeMessage() {
    var divWelcome = document.getElementById('WelcomeMessage');
    divWelcome.innerHTML = 'Welcome ' + myMSALObj.getAccount().userName + "to Microsoft Graph API";
    var loginbutton = document.getElementById('SignIn');
    loginbutton.innerHTML = 'Sign Out';
    loginbutton.setAttribute('onclick', 'signOut();');
}


//This function can be removed if you do not need to support IE
function acquireTokenRedirectAndCallMSGraph() {
     //Always start with acquireTokenSilent to obtain a token in the signed in user from cache
     myMSALObj.acquireTokenSilent(requestObj).then(function (tokenResponse) {
         console.log("93 tokenResponse ",tokenResponse)
         console.log("graphConfig.graphMeEndpoint ",graphConfig.graphMeEndpoint)
         callMSGraph(graphConfig.graphMeEndpoint, tokenResponse.accessToken, graphAPICallback);
     }).catch(function (error) {
         console.log(error);
         // Upon acquireTokenSilent failure (due to consent or interaction or login required ONLY)
         // Call acquireTokenRedirect
         if (requiresInteraction(error.errorCode)) {
             console.log("101 requestObj ",requestObj)
             myMSALObj.acquireTokenRedirect(requestObj);
         }
     });
}
function authRedirectCallBack(error, response) {
    if (error) {
        console.log(error);
    }
    else {
        console.log("104 response ",response)
        if (response.tokenType === "access_token") {
            callMSGraph(graphConfig.graphEndpoint, response.accessToken, graphAPICallback);
        } else {
            console.log("token type is:" + response.tokenType);
        }
    }
}

function requiresInteraction(errorCode) {
    if (!errorCode || !errorCode.length) {
        return false;
    }
    return errorCode === "consent_required" || errorCode === "interaction_required" || errorCode === "login_required";
}


// Browser check variables
var ua = window.navigator.userAgent;
var msie = ua.indexOf('MSIE ');
var msie11 = ua.indexOf('Trident/');
var msedge = ua.indexOf('Edge/');
var isIE = msie > 0 || msie11 > 0;
var isEdge = msedge > 0;
//If you support IE, our recommendation is that you sign-in using Redirect APIs
//If you as a developer are testing using Edge InPrivate mode, please add "isEdge" to the if check
// can change this to default an experience outside browser use
//var loginType = isIE ? "REDIRECT" : "POPUP";
var loginType = "REDIRECT";
//var loginType = "POPUP";
if (loginType === 'POPUP') {
     if (myMSALObj.getAccount()) {// avoid duplicate code execution on page load in case of iframe and popup window.
         showWelcomeMessage();
         acquireTokenPopupAndCallMSGraph();
     }
}
else if (loginType === 'REDIRECT') {
    document.getElementById("SignIn").onclick = function () {
         myMSALObj.loginRedirect(requestObj);
    };
    if (myMSALObj.getAccount() && !myMSALObj.isCallback(window.location.hash)) {// avoid duplicate code execution on page load in case of iframe and popup window.
         showWelcomeMessage();
         acquireTokenRedirectAndCallMSGraph();
     }
} else {
    console.error('Please set a valid login type');
}

function signOut() {
    myMSALObj.logout();
}