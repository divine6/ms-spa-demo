const express = require("express")
const app = express()
console.log("__dirname ",__dirname)
const router = express.Router()
console.log("router ",router)
const hbs = require("hbs");

app.set("view engine","hbs");

const PORT = 3600
router.use(express.static(__dirname+"/static"));

router.get("/",(req,res)=>{
    console.log("imssodemo test:get /")
    res.render("index.hbs")
})
router.get("/logout",(req,res)=>{
    console.log("imssodemo test:get /logout")
    res.render("logout.hbs")
})
router.get("/msredirect",(req,res)=>{
    console.log("imssodemo test:get /msredirect")
    res.render("msredirect.hbs")
})
app.use("/imsso",router);

app.listen(PORT,()=>{
    console.log("imssodemo app started on port ",PORT)
})